/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;
import gradebook.dao.*;
import attendance.entity.*;
import java.sql.Date;
import java.util.Scanner;
/**
 *
 * @author ahkhan.bscs13seecs
 */
public class app {
    
    public static void main(String [] args)
    {
        System.out.println("Welcome!\n Select from these options...");
        
        Scanner scanner = new Scanner(System.in);
        while(true)
        {
            System.out.println("1) Add Course");
            
            
            switch(scanner.nextInt())
            {
                case 1:
                {
                    Course course = new Course();
                    Teacher teacher = new Teacher();
                    
                    System.out.println("Please enter name of course...");
                    course.setClasstitle(scanner.next());
                    
                    System.out.println("Please enter name of course teacher...");
                    teacher.setFullname(scanner.next());
                    
                    System.out.println("Please enter email of course teacher...");
                    teacher.setEmail(scanner.next());
                    
                    teacher.setPosition("random");
                    
                    course.setTeacher(teacher);
                    
                    course.setCreditHours(3);
                    course.setEndtime(new Date(1,1,1));
                    course.setStarttime(new Date(1,1,1));
                    
                    course.save();
                }
            }
        }
    }
}
